import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import 'rxjs/add/operator/delay';

import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient,
      @Inject('LOCALSTORAGE') private localStorage: Storage) {
  }

  login(email: string, password: string) {

      const credentials = {
          email: email,
          password: password
      };

      return this.http.post(environment.host + '/api/v1/signin', credentials)
          .pipe(map((response) => {
              console.log('response', response);
              // set token property
              // const decodedToken = jwt_decode(response['access_token']);

              // store email and jwt token in local storage to keep user logged in between page refreshes
              this.localStorage.setItem('currentUser', JSON.stringify({
                  token: response['access_token'],
                  isAdmin: response['is_admin'],
                  email: response['email'],
                  id: response['user_id'],
                  expiration: moment().add(1, 'days').toDate(),
                  fullName: response['full_name']
              }));

              return true;
          }));
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.localStorage.removeItem('currentUser');
    this.http.post(environment.host + '/api/v1/signout', {})
      .pipe(map((response) => {
          return true;
      }));
  }

  signup(email: string, password: string, full_name: string) {

    const user = {
      email: email,
      password: password,
      full_name: full_name
    };

    return this.http.post(environment.host + '/api/v1/signup', user, {headers: {'Content-Type': 'application/json'}})
      .pipe(map((response) => {
        console.log('response', response);
        return true;
      }));
  }

  getCurrentUser(): any {
      return JSON.parse(this.localStorage.getItem('currentUser'));
  }
}
