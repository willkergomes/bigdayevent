import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/delay';

import { environment } from '../../../environments/environment';
import { ApiListResponse } from '../../shared/models/api-list-response';
import { UserListItem } from '../../shared/models/user-list-item';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ImageListItem } from '../../shared/models/image-list-item';

@Injectable({
    providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  getAll(page = 1, perPage = 5): Observable<ApiListResponse<UserListItem>> {
    return this.http.get(
      `${environment.host}/api/v1/users?page=${page}&per_page=${perPage}`
    ) as Observable<ApiListResponse<UserListItem>>;
  }

  delete(user_id: number) {
    if (user_id) {
      return this.http.delete(
        `${environment.host}/api/v1/users/${user_id}`
      )
        .pipe(map((response) => {
          return true;
        })) as Observable<boolean>;
    }
    return of(false);
  }
}
