import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewFabButtonService {

  visibility: BehaviorSubject<boolean>;

  private actionSubject: BehaviorSubject<any>;

  constructor() {
    this.visibility = new BehaviorSubject(false);
    this.actionSubject = new BehaviorSubject(false);
  }
  sendClickEvent(value: any) {
    this.actionSubject.next(value);
  }

  getClickEvent(): Observable<any> {
    return this.actionSubject.asObservable();
  }

  show() {
    this.visibility.next(true);
  }

  hide() {
    this.visibility.next(false);
  }
}
