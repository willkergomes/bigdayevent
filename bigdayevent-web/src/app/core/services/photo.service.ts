import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/delay';

import { environment } from '../../../environments/environment';
import { ImageListItem } from '../../shared/models/image-list-item';
import { NGXLogger } from 'ngx-logger';
import { Observable, of, Subscription } from 'rxjs';
import { ApiListResponse } from '../../shared/models/api-list-response';

@Injectable({
    providedIn: 'root'
})
export class PhotoService {

    uploadSub: Subscription;

    constructor(private http: HttpClient,
                private logger: NGXLogger) {
    }

    getAll(page = 1, perPage = 5, toApprove= false): Observable<ApiListResponse<ImageListItem>> {
        return this.http.get(`${environment.host}/api/v1/photos?page=${page}&per_page=${perPage}&to_approve=${toApprove}`)
          .pipe(map((response) => {
            const photos = response['items'] as ImageListItem[];
            photos.forEach(value => {
                value.src = `${environment.s3_prefix}${value.object_key}`;
                value.caption = value.file_name;
            });
            return response;
          })) as Observable<ApiListResponse<ImageListItem>>;
    }

    fileUpload(file: File) {
        const formData = new FormData();
        formData.append('image', file);

        return this.http.post(environment.host + '/api/v1/photos', formData);
    }

    approve(photo_id: number) {
        if (photo_id) {
            return this.http.patch(
              `${environment.host}/api/v1/photos/${photo_id}/approve`, {}
            )
              .pipe(map((response) => {
                  return response as ImageListItem;
              })) as Observable<ImageListItem>;
        }
        return of(null);
    }

    delete(photo_id: number) {
        if (photo_id) {
            return this.http.delete(
              `${environment.host}/api/v1/photos/${photo_id}`
            )
              .pipe(map((response) => {
                  return true;
              })) as Observable<boolean>;
        }
        return of(false);
    }
}
