import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { NotificationService } from '../../core/services/notification.service';
import { NGXLogger } from 'ngx-logger';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { NewFabButtonService } from '../../core/services/new-fab-button.service';
import { UserListItem } from '../../shared/models/user-list-item';
import { UserService } from '../../core/services/user.service';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  displayedColumns: string[] = ['id', 'email', 'name', 'createdAt', 'confirmed', 'isAdmin', 'action'];
  dataSource: MatTableDataSource<UserListItem>;

  pageEvent: PageEvent;
  dataLength: number;
  pageIndex = 1;
  pageSize = 5;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private logger: NGXLogger,
    private notificationService: NotificationService,
    private newFabButtonService: NewFabButtonService,
    private titleService: Title,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Big Day - Users');
    this.logger.log('Users loaded');
    this.newFabButtonService.hide();
    this.getAllUsers();
  }

  getAllUsers(pageIndex = 0, perPage = 5) {
    this.userService.getAll(pageIndex + 1, perPage).subscribe(response => {
      this.dataSource = new MatTableDataSource(response.items);
      this.pageSize = response.items.length;
      this.pageIndex = pageIndex;
      this.dataLength = response.total;
      this.dataSource.sort = this.sort;
    },
      error => {
        // handle error
      });
  }

  clickRefresh() {
    this.getAllUsers(this.pageIndex, this.pageSize);
  }

  paginate(event?: PageEvent) {
    this.getAllUsers(event.pageIndex, event.pageSize);
    return event;
  }

  clickDeleteUser(event, user, index) {
    this.userService.delete(user.user_id).subscribe(value => {
      if (value) {
        this.notificationService.openSnackBar(`User ${user.email} deleted!`);
        this.dataSource.data.slice(index, 1);
        this.clickRefresh();
      } else {
        this.notificationService.openSnackBar(`Error when deleting user ${user.email}!`);
      }
    });
  }
}
