import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoRoutingModule } from './photo-routing.module';
import { SharedModule } from '../shared/shared.module';
import { PhotoUploadComponent } from './photo-upload/photo-upload.component';

@NgModule({
  imports: [
    CommonModule,
    PhotoRoutingModule,
    SharedModule
  ],
  declarations: [
    PhotoUploadComponent
  ],
  entryComponents: [
  ]
})
export class PhotoModule { }
