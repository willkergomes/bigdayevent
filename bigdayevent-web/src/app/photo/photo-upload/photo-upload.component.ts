import { Component, OnInit } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { Title } from '@angular/platform-browser';

import { NotificationService } from '../../core/services/notification.service';
import { PhotoService } from '../../core/services/photo.service';
import { Router } from '@angular/router';
import { SpinnerService } from '../../core/services/spinner.service';
import { NewFabButtonService } from '../../core/services/new-fab-button.service';

@Component({
  selector: 'app-photo-upload',
  templateUrl: './photo-upload.component.html',
  styleUrls: ['./photo-upload.component.css']
})
export class PhotoUploadComponent implements OnInit {

  fileName = '';

  constructor(
    private logger: NGXLogger,
    private notificationService: NotificationService,
    private newFabButtonService: NewFabButtonService,
    private spinnerService: SpinnerService,
    private photoService: PhotoService,
    private router: Router,
    private titleService: Title
  ) { }

  ngOnInit() {
    this.titleService.setTitle('Big Day - Photo Upload');
    this.logger.log('Photo Upload loaded');
    this.newFabButtonService.hide();
  }

  onFileSelected(event) {
    const file: File = event.target.files[0];
    if (file) {
      if (file.size > 4072000) {
        this.notificationService.openSnackBar(`File too big! Maximum 4Mb!`);
        return false;
      }

      this.fileName = file.name;
      this.spinnerService.show();

      this.photoService.fileUpload(file).subscribe(
        (data) => {
          this.logger.info(`File ${this.fileName} uploaded!`);
          this.router.navigate(['/dashboard']).then(() => {
            this.notificationService.openSnackBar(`File saved!`);
          });
          this.spinnerService.hide();
        },
        error => {
          this.spinnerService.hide();
          this.fileName = '';
          this.notificationService.openSnackBar(error.error.message ? error.error.message : error.error);
        });
    }
  }
}
