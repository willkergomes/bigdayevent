export interface UserListItem {
  user_id: number;
  email: string;
  full_name: string;
  confirmed: boolean;
  is_admin: string;
  created_at: Date;
}
