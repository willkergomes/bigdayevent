import { UserListItem } from './user-list-item';

export interface ImageListItem {
  id: number;
  src: string;
  caption: string;
  approved: boolean;
  created_at: string;
  file_name: string;
  photo_id: number;
  object_key: string;
  owrner: UserListItem;
}
