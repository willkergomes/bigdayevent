export interface ApiListResponse<T> {
  items: T[];
  total: number;
  has_next: boolean;
  has_prev: boolean;
  next_num: number;
  pages: number;
  per_page: number;
  prev_num: number;
}
