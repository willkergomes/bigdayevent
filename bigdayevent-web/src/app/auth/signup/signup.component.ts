import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../core/services/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from '../../core/services/notification.service';
import { SpinnerService } from '../../core/services/spinner.service';
import { NGXLogger } from 'ngx-logger';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  form: FormGroup;
  fullName: string;
  email: string;
  hideNewPassword: boolean;
  newPassword: string;
  newPasswordConfirm: string;
  disableSubmit: boolean;

  constructor(private router: Router,
              private authService: AuthenticationService,
              private logger: NGXLogger,
              private spinnerService: SpinnerService,
              private notificationService: NotificationService) {

    this.hideNewPassword = true;
  }

  ngOnInit() {
    this.form = new FormGroup({
      fullName: new FormControl('', [
        Validators.required, Validators.minLength(3), Validators.maxLength(100)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      newPassword: new FormControl('', [
        Validators.required, Validators.minLength(4), Validators.maxLength(10)]),
      newPasswordConfirm: new FormControl('', Validators.required),
    });

    this.form.get('fullName').valueChanges
      .subscribe(val => { this.fullName = val; });

    this.form.get('email').valueChanges
      .subscribe(val => { this.email = val; });

    this.form.get('newPassword').valueChanges
      .subscribe(val => { this.newPassword = val; });

    this.form.get('newPasswordConfirm').valueChanges
      .subscribe(val => { this.newPasswordConfirm = val; });

    this.spinnerService.visibility.subscribe((value) => {
      this.disableSubmit = value;
    });
  }

  signup() {
    if (this.newPassword !== this.newPasswordConfirm) {
      this.notificationService.openSnackBar('New passwords do not match.');
      return;
    }

    const email = this.email;

    this.authService.signup(email, this.newPassword, this.fullName)
      .subscribe(
        data => {
          this.logger.info(`User ${email} created!`);
          this.form.reset();
          this.router.navigate(['/auth/login']).then(value => {
            this.notificationService.openSnackBar(
              `We sent a confirmation to ${email} mail box! Please check and confirm!`,
              15000);
          });
        },
        error => {
          this.notificationService.openSnackBar(error.error.message ? error.error.message : error.error);
        }
      );
  }

}
