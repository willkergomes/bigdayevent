import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Title } from '@angular/platform-browser';
import { NGXLogger } from 'ngx-logger';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NewFabButtonService } from '../../core/services/new-fab-button.service';
import { PhotoService } from '../../core/services/photo.service';
import { ImageListItem } from '../../shared/models/image-list-item';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatSlideToggleChange, PageEvent } from '@angular/material';

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.css']
})
export class DashboardHomeComponent implements OnInit, OnDestroy {
  currentUser: any;
  onlyToApprove = false;
  photos = [] as ImageListItem[];

  @Output()
  reloadEvent = new EventEmitter<ImageListItem>();

  $reloadEventSubscription: Subscription;
  $clickFabEventSubscription: Subscription;
  $newFabButtonSubscription: Subscription;
  $getAllPhotoServiceSubscription: Subscription;
  newFabButtonVisible;

  isAdmin = false;

  pageEvent: PageEvent;
  dataLength: number;
  pageIndex = 1;
  pageSize = 5;

  constructor(private notificationService: NotificationService,
              private authService: AuthenticationService,
              private newFabButtonService: NewFabButtonService,
              private photoService: PhotoService,
              private titleService: Title,
              private router: Router,
              private logger: NGXLogger) {
  }

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();
    this.isAdmin = this.currentUser.isAdmin;
    this.titleService.setTitle('Jack & Jill Wedding Big Day - Gallery');
    this.logger.log('Dashboard loaded');

    this.$newFabButtonSubscription = this.newFabButtonService.visibility.subscribe((value) => {
      this.newFabButtonVisible = value;
    });

    this.$clickFabEventSubscription = this.newFabButtonService.getClickEvent().subscribe((value) => {
      if (this.newFabButtonVisible && value) {
        this.newFabButtonService.hide();
        this.router.navigate(['/upload']).then(() => {
          this.logger.log('Upload a photo view!');
        });
      }
    });

    this.getAllPhotos();

    this.$reloadEventSubscription = this.reloadEvent.subscribe(event => {
      if (event) {
        this.getAllPhotos();
      }
    });

    setTimeout(() => {
      this.notificationService.openSnackBar('Welcome!');
    });
  }

  paginate(event?: PageEvent) {
    this.getAllPhotos(event.pageIndex, event.pageSize);
    return event;
  }

  getAllPhotos(pageIndex = 0, perPage = 5) {
    if (this.$getAllPhotoServiceSubscription) {
      this.$getAllPhotoServiceSubscription.unsubscribe();
    }
    this.$getAllPhotoServiceSubscription = this.photoService.getAll(pageIndex + 1, perPage, this.onlyToApprove)
      .subscribe(response => {
        this.photos = response.items;
        this.pageSize = response.items.length;
        this.pageIndex = pageIndex;
        this.dataLength = response.total;
        this.newFabButtonService.show();
    });
  }

  toggle(event: MatSlideToggleChange) {
    console.log('Toggle fired');
    this.onlyToApprove = event.checked;
    this.getAllPhotos();
  }

  public ngOnDestroy(): void {
    this.$reloadEventSubscription.unsubscribe();
    this.$newFabButtonSubscription.unsubscribe();
    this.$clickFabEventSubscription.unsubscribe();
    this.$getAllPhotoServiceSubscription.unsubscribe();
  }
}
