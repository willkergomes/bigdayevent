import { Component, EventEmitter, Input, OnInit, } from '@angular/core';

import { ImageListItem } from '../../shared/models/image-list-item';
import { AuthenticationService } from '../../core/services/auth.service';
import { PhotoService } from '../../core/services/photo.service';
import { NotificationService } from '../../core/services/notification.service';
import { ConfirmDialogComponent, ConfirmDialogModel } from '../../shared/confirm-dialog/confirm-dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss']
})
export class ImageListComponent implements OnInit {
  @Input()
  images: ImageListItem[] = [];

  @Input()
  reloadEvent: EventEmitter<ImageListItem>;

  isAdmin = false;

  constructor(private authService: AuthenticationService,
              private notificationService: NotificationService,
              private photoService: PhotoService,
              private dialog: MatDialog) {
  }

  public ngOnInit(): void {
    const user = this.authService.getCurrentUser();
    this.isAdmin = user.isAdmin;
  }

  clickApprovePhoto(event, photo: ImageListItem, index: number) {
    const dialogConfig = new MatDialogConfig<ConfirmDialogModel>();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Confirm Dialog',
      message: `Do you confirm to turn public ${photo.file_name} photo?`,
      subject: photo
    } as ConfirmDialogModel;

    const $dialogSub = this.dialog.open(ConfirmDialogComponent, dialogConfig).afterClosed();
    $dialogSub.subscribe(photoApproved => {
      if (photoApproved) {
        this.photoService.approve(photo.photo_id).subscribe(value => {
          if (value) {
            this.notificationService.openSnackBar(`Photo ${photoApproved.file_name} approved!`);
            this.reloadEvent.emit(photoApproved);
          } else {
            this.notificationService.openSnackBar(`Error when approving photo ${photoApproved.file_name}}!`);
          }
        });
      }
    });
  }

  clickDeletePhoto(event, photo: ImageListItem, index: number) {
    const dialogConfig = new MatDialogConfig<ConfirmDialogModel>();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      title: 'Confirm Dialog',
      message: `Do you confirm to delete ${photo.file_name} photo?`,
      subject: photo,
      isWarn: true
    } as ConfirmDialogModel;

    const $dialogSub = this.dialog.open(ConfirmDialogComponent, dialogConfig).afterClosed();
    $dialogSub.subscribe(photoDeleted => {
      if (photoDeleted) {
        this.photoService.delete(photoDeleted.photo_id).subscribe(value => {
          if (value) {
            this.notificationService.openSnackBar(`Photo ${photoDeleted.file_name} deleted!`);
            this.reloadEvent.emit(photoDeleted);
          } else {
            this.notificationService.openSnackBar(`Error when deleting photo ${photoDeleted.file_name}!`);
          }
        });
      }
    });
  }
}
