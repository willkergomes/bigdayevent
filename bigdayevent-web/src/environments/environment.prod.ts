import { NgxLoggerLevel } from 'ngx-logger';

export const environment = {
  production: true,
  logLevel: NgxLoggerLevel.OFF,
  serverLogLevel: NgxLoggerLevel.ERROR,
  s3_prefix: 'https://bigdayevent.s3.us-east-2.amazonaws.com/',
  host: 'https://bigdayevent.herokuapp.com'
};
