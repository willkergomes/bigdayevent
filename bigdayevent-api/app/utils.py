import os
import uuid

from werkzeug.utils import import_string

CONFIG_NAME_MAPPER = {
    'development': 'config.DevelopmentConfig',
    'production': 'config.ProductionConfig',
    'testing': 'config.TestingConfig',
}


def get_config(config_name=None):
    flask_config_name = os.getenv('FLASK_CONFIG', 'development')
    if config_name is not None:
        flask_config_name = config_name
    return import_string(CONFIG_NAME_MAPPER[flask_config_name])


def perform_file_name(file_name):
    return ''.join([str(uuid.uuid4().hex[:6]), file_name])


def create_random_token():
    return ''.join([str(uuid.uuid4().hex)])
