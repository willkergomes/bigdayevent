import os
import uuid
from http import HTTPStatus

import boto3


class S3Storage:
    AWS_CLIENT_TYPE = 's3'

    def __init__(self, bucket: str = None):
        if not bucket:
            self.bucket = os.environ.get("S3_BUCKET_NAME")
        if not self.bucket:
            raise ValueError("No S3_BUCKET_NAME set for application")

    @classmethod
    def create_object_key(cls, file_name):
        # Create Object Key for best performance in S3
        return ''.join([str(uuid.uuid4().hex[:6]), file_name])

    def upload_file(self, file_name: str, object_key):
        s3_client = boto3.client(S3Storage.AWS_CLIENT_TYPE)
        s3_client.upload_file(file_name, self.bucket, object_key)

    def turn_file_public(self, file_name: str):
        s3 = boto3.resource(S3Storage.AWS_CLIENT_TYPE)
        object_acl = s3.ObjectAcl(self.bucket, file_name)
        response = object_acl.put(ACL='public-read')
        if not response or response.get('ResponseMetadata').get('HTTPStatusCode') != HTTPStatus.OK:
            raise Exception('Error while changing file policy on storage.')

    def delete_file(self, file_name: str):
        s3 = boto3.resource(S3Storage.AWS_CLIENT_TYPE)
        obj_storage = s3.Object(self.bucket, file_name)
        response = obj_storage.delete()
        if not response or response.get('ResponseMetadata').get('HTTPStatusCode') != HTTPStatus.NO_CONTENT:
            raise Exception('Error while deleting file on storage.')
