import boto3
from botocore.exceptions import ClientError


class EmailSender:
    AWS_CLIENT_TYPE = 'ses'
    AWS_REGION = "us-east-2"
    CHARSET = "UTF-8"
    # CONFIGURATION_SET = "ConfigSet"
    SENDER = "NO REPLY <willker.gomes@gmail.com>"

    BODY_HTML = """<html>
    <head></head>
    <body>
    {}
    </body>
    </html>
                """

    def __init__(self, subject: str, text: str, to_email):
        self.subject = subject
        self.text = text
        self.to_email = to_email

    def send(self):
        # Create a new SES resource and specify a region.
        client = boto3.client('ses', region_name=EmailSender.AWS_REGION)

        # Try to send the email.
        try:
            # Provide the contents of the email.
            response = client.send_email(
                Destination={
                    'ToAddresses': [
                        self.to_email,
                    ],
                },
                Message={
                    'Body': {
                        'Html': {
                            'Charset': EmailSender.CHARSET,
                            'Data': EmailSender.BODY_HTML.format(self.text),
                        },
                        'Text': {
                            'Charset': EmailSender.CHARSET,
                            'Data': self.text,
                        },
                    },
                    'Subject': {
                        'Charset': EmailSender.CHARSET,
                        'Data': self.subject,
                    },
                },
                Source=EmailSender.SENDER,
                # If you are not using a configuration set, comment or delete the
                # following line
                # ConfigurationSetName=EmailSender.CONFIGURATION_SET,
            )
            return response
        # Display an error if something goes wrong.
        except ClientError as e:
            raise ValueError(e.response['Error']['Message'])
