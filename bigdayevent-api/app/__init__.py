
from http import HTTPStatus
from flask import Flask, jsonify, send_from_directory
from flask_cors import CORS
from flask_restful import Api
from flask_jwt_extended import JWTManager

from app.database import init_db
from app.resources import init_resources
from app.utils import get_config
from blacklist import BLACKLIST

app = Flask(__name__, static_folder='static', static_url_path='')

try:
    app.config.from_object(get_config())
except ImportError:
    raise Exception('Invalid Config')


cors = CORS(app)

api = Api(app)
jwt = JWTManager(app)


@app.route('/')
def index():
    return send_from_directory(app.static_folder, 'index.html')


@jwt.token_in_blocklist_loader
def check_blacklist(self, token):
    return token['jti'] in BLACKLIST


@jwt.revoked_token_loader
def access_token_invalidated(jwt_headers, jwt_payload):
    # return Response(jsonify({'message', 'You have been logged out.'}), mimetype="application/json", status=401)
    # return jsonify(msg=f"I'm sorry {jwt_payload['sub']} I can't let you do that"), 401
    return jsonify(message="You have been logged out."), HTTPStatus.UNAUTHORIZED


init_resources(api)

init_db(app)
