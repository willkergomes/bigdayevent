import traceback
from http import HTTPStatus

from flask import make_response, render_template, request
from flask_cors import cross_origin
from flask_jwt_extended import create_access_token, get_jwt
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource
from flask_restful import reqparse
from psycopg2 import OperationalError

from app.models.user_model import UserModel
from app.utils import create_random_token
from blacklist import BLACKLIST


class UsersResource(Resource):
    # GET /api/v1/users
    # @app.route('/api/v1/users', methods=['GET'])
    @cross_origin()
    @jwt_required()
    def get(self):
        logged_user = UserModel.find_by_id(get_jwt_identity())
        page = request.args.get('page', 1)
        per_page = request.args.get('per_page', 5)
        if not logged_user.is_admin:
            return {'message': 'User do not have permission.'}, HTTPStatus.UNAUTHORIZED
        items = []
        result = UserModel.find_all(int(page), int(per_page))
        if result:
            for item in result.items:
                items.append(item.json())
            return {'items': items,
                    'total': result.total,
                    'has_next': result.has_next,
                    'has_prev': result.has_prev,
                    'next_num': result.next_num,
                    'pages': result.pages,
                    'per_page': result.per_page,
                    'prev_num': result.prev_num}, HTTPStatus.OK
        return {'items': items, 'total': 0}, HTTPStatus.OK


class UserResource(Resource):
    # GET /api/v1/users
    # @app.route('/api/v1/users', methods=['GET'])
    @cross_origin()
    @jwt_required()
    def get(self, user_id):
        logged_user = UserModel.find_by_id(get_jwt_identity())
        if not logged_user.is_admin:
            return {'message': 'User do not have permission.'}, HTTPStatus.UNAUTHORIZED
        user = UserModel.find_by_id(user_id)
        if user:
            return user.json(), HTTPStatus.OK  # Success
        return {'message': 'User not found.'}, HTTPStatus.NOT_FOUND  # not found

    # DELETE /api/v1/users/{user_id}
    # @app.route('/api/v1/users/<int:user_id>', methods=['DELETE'])
    @cross_origin()
    @jwt_required()
    def delete(self, user_id):
        logged_user = UserModel.find_by_id(get_jwt_identity())
        if not logged_user.is_admin:
            return {'message': 'User do not have permission.'}, HTTPStatus.UNAUTHORIZED
        user = UserModel.find_by_id(user_id)
        if user:
            try:
                user.delete_user()
            except:
                return {'message': 'An internal error ocurred trying to delete user.'}, HTTPStatus.INTERNAL_SERVER_ERROR
            return {'message': 'User deleted.'}, HTTPStatus.NO_CONTENT
        return {'message': 'User not found.'}, HTTPStatus.NOT_FOUND


class UserSignup(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('email', type=str, required=True, help="The field 'email' cannot be left blank")
        self.parser.add_argument('password', type=str, required=True, help="The field 'password' cannot be left blank")
        self.parser.add_argument('full_name', type=str, required=True,
                                 help="The field 'full_name' cannot be left blank")

    # POST /api/v1/signup
    # @app.route('/api/v1/signup', methods=['POST'])
    @cross_origin()
    def post(self):
        data = self.parser.parse_args()
        email = data.get('email')

        if UserModel.find_by_email(email):
            return {"message": "The email '{}' already exists.".format(email)}, HTTPStatus.BAD_REQUEST

        user = UserModel(confirmation_token=create_random_token(), **data)
        user.save_user()
        try:
            user.send_confirmation_email()
        except:
            user.delete_user()
            traceback.print_exc()
            return {"message": "An internal server error has occurred."}, HTTPStatus.INTERNAL_SERVER_ERROR
        return {'message': 'User created successfully.'}, HTTPStatus.CREATED


class UserSignin(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('email', type=str, required=True, help="The field 'email' cannot be left blank")
        self.parser.add_argument('password', type=str, required=True, help="The field 'password' cannot be left blank")

    # POST /api/v1/signin
    # @app.route('/api/v1/signin', methods=['POST'])
    @cross_origin()
    def post(self):
        data = self.parser.parse_args()

        try:
            user = UserModel.find_by_email(data['email'])
        except OperationalError: # retry for lost connection
            user = UserModel.find_by_email(data['email'])

        if user and user.validate_password(data['password']):
            if user.confirmed:
                token_de_acesso = create_access_token(identity=user.id)
                return {'access_token': token_de_acesso, **user.json()}, HTTPStatus.OK
            return {'message': 'User not confirmed. Check your mail box.'}, HTTPStatus.BAD_REQUEST
        return {'message': 'The username or password is incorrect.'}, HTTPStatus.UNAUTHORIZED


class UserSignout(Resource):
    @jwt_required()
    def post(self):
        jwt_id = get_jwt()['jti']  # JWT Token identifier
        BLACKLIST.add(jwt_id)
        return {'message': 'Logged out successfully!'}, HTTPStatus.OK


class UserConfirm(Resource):

    # GET /api/v1/users/{confirmation_token}/confirm
    # @app.route('/api/v1/<str:confirmation_token>/confirm', methods=['GET'])
    @classmethod
    def get(cls, confirmation_token):
        user = UserModel.find_by_confirmation_token(confirmation_token)
        if not user:
            return {"message": "Invalid confirmation token."}, HTTPStatus.BAD_REQUEST

        user.confirmed = True
        try:
            user.save_user()
            headers = {'Content-Type': 'text/html'}
            return make_response(
                render_template('user_confirm.html', email=user.email, usuario=user.full_name), HTTPStatus.OK, headers
            )
        except:
            traceback.print_exc()
            return {"message": "An internal server error has occurred."}, HTTPStatus.INTERNAL_SERVER_ERROR
