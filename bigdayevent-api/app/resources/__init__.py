from app.resources.photo_resource import *
from app.resources.user_resource import *


API_V1 = 'v1'
API_PATH = f'/api/{API_V1}'


def init_resources(api):
    api.add_resource(PhotoApprovalResource, f'{API_PATH}/photos/<string:photo_id>/approve')
    api.add_resource(PhotoResource, f'{API_PATH}/photos/<string:photo_id>')
    api.add_resource(PhotoUploadResource, f'{API_PATH}/photos')
    api.add_resource(PhotosResource, f'{API_PATH}/photos')
    api.add_resource(UserSignin, f'{API_PATH}/signin')
    api.add_resource(UserSignup, f'{API_PATH}/signup')
    api.add_resource(UserSignout, f'{API_PATH}/signout')
    api.add_resource(UserResource, f'{API_PATH}/users/<int:user_id>')
    api.add_resource(UserConfirm, f'{API_PATH}/users/<string:confirmation_token>/confirm')
    api.add_resource(UsersResource, f'{API_PATH}/users')
