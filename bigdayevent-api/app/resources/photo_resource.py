import math
import os
import tempfile
from http import HTTPStatus

from PIL import Image
from flask import request
from flask_cors import cross_origin
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource

from app.models.photo_model import PhotoModel
from app.models.user_model import UserModel
from app.utils import perform_file_name


class PhotosResource(Resource):
    # GET /api/v1/photos
    # @app.route('/api/v1/photos', methods=['GET'])
    @cross_origin()
    @jwt_required()
    def get(self):
        logged_user = UserModel.find_by_id(get_jwt_identity())
        page = request.args.get('page', 1)
        per_page = request.args.get('per_page', 5)
        to_approve = request.args.get('to_approve', False)
        items = []
        if logged_user.is_admin:
            if to_approve and (to_approve == 'true' or to_approve == 'True'):
                result = PhotoModel.find_by_approved(is_approved=False, page=int(page), per_page=int(per_page))
            else:
                result = PhotoModel.find_all(page=int(page), per_page=int(per_page))
        else:
            result = PhotoModel.find_by_approved(page=int(page), per_page=int(per_page))

        if result:
            for item in result.items:
                items.append(item.json())
            return {'items': items,
                    'total': result.total,
                    'has_next': result.has_next,
                    'has_prev': result.has_prev,
                    'next_num': result.next_num,
                    'pages': result.pages,
                    'per_page': result.per_page,
                    'prev_num': result.prev_num}, HTTPStatus.OK
        return {'items': items, 'total': 0}, HTTPStatus.OK


class PhotoUploadResource(Resource):

    # POST /api/v1/photos
    # @app.route('/api/v1/photos', methods=['POST'])
    @cross_origin()
    @jwt_required()
    def post(self):
        image_file = request.files.get("image", None)
        if image_file:
            performed_file_name = perform_file_name(image_file.filename)
            path_to_image = os.path.join(tempfile.gettempdir(), performed_file_name)
            image_file.save(path_to_image)

            image = Image.open(path_to_image)
            x, y = image.size
            x2, y2 = math.floor(x - 50), math.floor(y - 20)
            image.thumbnail((x2, y2))
            # foo = image.resize((x2, y2), Image.ANTIALIAS)

            # performed_file_name = perform_file_name(image_file.filename)
            # path_to_image = os.path.join(tempfile.gettempdir(), performed_file_name)
            image.save(path_to_image)
            # foo.save(path_to_image, quality=95)

            user = UserModel.find_by_id(get_jwt_identity())
            photo = PhotoModel(image_file.filename, performed_file_name, user.id)
            try:
                photo.save_photo()
            except:
                return {'message': 'An internal error ocurred trying to save photo.'}, HTTPStatus.INTERNAL_SERVER_ERROR
            return photo.json(), HTTPStatus.CREATED
        return {'message': 'Invalid photo image.'}, HTTPStatus.BAD_REQUEST


class PhotoApprovalResource(Resource):

    # PATCH /api/v1/photos/{photo_id}/approve
    # @app.route('/api/v1/photos/{photo_id}/approve', methods=['POST'])
    @cross_origin()
    @jwt_required()
    def patch(self, photo_id):
        logged_user = UserModel.find_by_id(get_jwt_identity())
        if not logged_user.is_admin:
            return {'message': 'User do not have permission.'}, HTTPStatus.UNAUTHORIZED
        photo = PhotoModel.find_by_id(photo_id)
        if photo:
            try:
                photo.approve_photo()
            except:
                return {'message': 'An internal error ocurred trying to approve photo.'}, \
                       HTTPStatus.INTERNAL_SERVER_ERROR
            return photo.json(), HTTPStatus.OK
        return {'message': 'Photo not found.'}, HTTPStatus.NOT_FOUND


class PhotoResource(Resource):

    # GET /api/v1/photos/{photo_id}
    # @app.route('/api/v1/photos/<int:photo_id>', methods=['GET'])
    @cross_origin()
    @jwt_required()
    def get(self, photo_id):
        logged_user = UserModel.find_by_id(get_jwt_identity())
        photo = PhotoModel.find_by_id(photo_id)
        if photo and (photo.approved or logged_user.is_admin):
            return photo.json(), HTTPStatus.OK
        return {'message': 'Photo not found.'}, HTTPStatus.NOT_FOUND

    # DELETE /api/v1/photos/{photo_id}
    # @app.route('/api/v1/photos/<int:photo_id>', methods=['DELETE'])
    @cross_origin()
    @jwt_required()
    def delete(self, photo_id):
        logged_user = UserModel.find_by_id(get_jwt_identity())
        if not logged_user.is_admin:
            return {'message': 'User do not have permission.'}, HTTPStatus.UNAUTHORIZED
        photo = PhotoModel.find_by_id(photo_id)
        if photo:
            try:
                photo.delete_photo()
            except:
                return {'message': 'An internal error ocurred trying to delete photo.'}, \
                       HTTPStatus.INTERNAL_SERVER_ERROR
            return {'message': 'Photo deleted.'}, HTTPStatus.NO_CONTENT
        return {'message': 'Photo not found.'}, HTTPStatus.NOT_FOUND
