# import os

from passlib.hash import bcrypt

from flask import request, url_for
# from requests import post

from app.database import db
from app.models.base_model import BaseModel
from app.email_sender import EmailSender

# FROM_TITTLE = 'NO-REPLY'
# FROM_EMAIL = 'no-reply@bigdayevent.com'


class UserModel(BaseModel):
    __tablename__ = 'users'

    email = db.Column(db.String(255), nullable=False, unique=True, index=True)
    full_name = db.Column(db.String(100), nullable=False)
    password = db.Column(db.String(255), nullable=False)
    confirmation_token = db.Column(db.String(100), unique=True, index=True)
    confirmed = db.Column(db.Boolean, default=False)
    is_admin = db.Column(db.Boolean, default=False)

    def __init__(self, email, full_name, password, confirmation_token):
        self.email = email
        self.full_name = full_name
        self.password = bcrypt.encrypt(password)
        self.confirmation_token = confirmation_token

    def __repr__(self):
        return (
            '<{class_name}('
            'user_id={self.id}, '
            'email={self.email}, '
            'full_name={self.full_name}, '
            'confirmation_token={self.confirmation_token}, '
            'confirmed={self.confirmed})>'.format(
                class_name=self.__class__.__name__,
                self=self
            )
        )

    def json(self):
        return {
            'user_id': self.id,
            'email': self.email,
            'full_name': self.full_name,
            'confirmed': self.confirmed,
            'is_admin': self.is_admin,
            'created_at': self.created_at.isoformat()
        }

    @classmethod
    def find_all(cls, page=1, per_page=20):
        items = cls.query.filter_by().paginate(page=page, per_page=per_page)
        if items:
            return items
        return None

    @classmethod
    def find_by_id(cls, user_id):
        item = cls.query.filter_by(id=user_id).first()
        if item:
            return item
        return None

    @classmethod
    def find_by_email(cls, email: str):
        item = cls.query.filter_by(email=email).first()
        if item:
            return item
        return None

    @classmethod
    def find_by_confirmation_token(cls, confirmation_token):
        item = cls.query.filter_by(confirmation_token=confirmation_token).first()
        if item:
            return item
        return None

    def validate_password(self, password):
        return bcrypt.verify(password, self.password)

    def save_user(self):
        items = self.query.all()

        # TODO temporary logic for admin permissions
        if len(items) == 0:
            self.is_admin = True

        db.session.add(self)
        db.session.commit()

    def delete_user(self):
        db.session.delete(self)
        db.session.commit()

    def send_confirmation_email(self):
        # MAILGUN_DOMAIN = os.environ.get("MAILGUN_DOMAIN")
        # MAILGUN_API_KEY = os.environ.get("MAILGUN_API_KEY")
        # if not MAILGUN_DOMAIN or not MAILGUN_API_KEY:
        #     raise ValueError("No MAILGUN CONFIG set for application")

        link = request.url_root[:-1] + url_for('userconfirm', confirmation_token=self.confirmation_token)
        text = '<p> \
            Confirme seu cadastro clicando no link a seguir: <a href="{}">CONFIRMAR EMAIL</a> \
            </p>'.format(link)
        sender = EmailSender(subject='Confirmação de Cadastro', text=text, to_email=self.email)
        sender.send()
        # return post('https://api.mailgun.net/v3/{}/messages'.format(MAILGUN_DOMAIN),
        #             auth=('api', MAILGUN_API_KEY),
        #             data={
        #                 'from': '{} <{}>'.format(FROM_TITTLE, FROM_EMAIL),
        #                 'to': self.email,
        #                 'subject': 'Confirmação de Cadastro',
        #                 'text': 'Confirme seu cadastro clicando no link a seguir: {}'.format(link),
        #                 'html': '<html><p> \
        #                         Confirme seu cadastro clicando no link a seguir: <a href="{}">CONFIRMAR EMAIL</a> \
        #                         </p></html>'.format(link)
        #                 }
        #             )
