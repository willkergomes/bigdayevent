import os
import tempfile
import traceback
from datetime import datetime

from app.database import db
from app.models.base_model import BaseModel
from app.storage import S3Storage


class PhotoModel(BaseModel):
    __tablename__ = 'photos'

    file_name = db.Column(db.String(100), nullable=False)
    object_key = db.Column(db.String(100), nullable=False)
    approved = db.Column(db.Boolean(), default=False)
    created_by = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    owner = db.relationship('UserModel',
        backref=db.backref('photos', lazy=True))

    def __init__(self, file_name: str, object_key: str, created_by: int):
        self.file_name = file_name
        self.object_key = object_key
        self.created_by = created_by

    def __repr__(self):
        return (
            '<{class_name}('
            'photo_id={self.id}, '
            'file_name={self.file_name}, '
            'approved={self.approved}, '
            'object_key={self.object_key}, '
            'owner={self.owner})>'.format(
                class_name=self.__class__.__name__,
                self=self
            )
        )

    def json(self):
        return {
            'photo_id': self.id,
            'file_name': self.file_name,
            'object_key': self.object_key,
            'approved': self.approved,
            'created_at': self.created_at.isoformat(),
            'owner': self.owner.json()
        }

    @classmethod
    def find_all(cls, page=1, per_page=20):
        items = cls.query.filter_by().paginate(page=page, per_page=per_page)
        if items:
            return items
        return None

    @classmethod
    def find_by_approved(cls, is_approved=True, page=1, per_page=20):
        items = cls.query.filter_by(approved=is_approved).paginate(page=page, per_page=per_page)
        if items:
            return items
        return None

    @classmethod
    def find_by_id(cls, photo_id):
        item = cls.query.filter_by(id=photo_id).first()
        if item:
            return item
        return None

    def save_photo(self):
        self.created_at = datetime.now()
        try:
            storage = S3Storage()
            storage.upload_file(os.path.join(tempfile.gettempdir(), self.object_key), self.object_key)
            storage.turn_file_public(self.object_key)
            db.session.add(self)
            db.session.commit()
        except Exception:
            traceback.print_exc()
            db.session.rollback()
            raise

    def delete_photo(self):
        try:
            storage = S3Storage()
            storage.delete_file(self.object_key)
            db.session.delete(self)
            db.session.commit()
        except Exception:
            traceback.print_exc()
            db.session.rollback()
            raise

    def approve_photo(self):
        try:
            self.approved = True
            db.session.add(self)
            storage = S3Storage()
            storage.turn_file_public(self.object_key)
            db.session.commit()
        except Exception:
            traceback.print_exc()
            db.session.rollback()
            raise
