import os


class BaseConfig:

    __abstract__ = True

    DEBUG = False

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    if not SQLALCHEMY_DATABASE_URI:
        raise ValueError("No SQLALCHEMY_DATABASE_URI set for Flask application")
    # Fix for compatibility with Heroku Env like
    if SQLALCHEMY_DATABASE_URI.startswith('postgres://'):
        SQLALCHEMY_DATABASE_URI = SQLALCHEMY_DATABASE_URI.replace('postgres://', 'postgresql://')

    JWT_SECRET_KEY = os.environ.get("JWT_SECRET_KEY", "DontTellAnyone")
    JWT_BLACKLIST_ENABLED = True


class DevelopmentConfig(BaseConfig):
    ENV = 'development'
    DEBUG = True
    DOMAIN = 'http://localhost:5000'


class ProductionConfig(BaseConfig):
    ENV = 'production'
    DOMAIN = 'http://localhost:5000'


class TestingConfig(BaseConfig):
    ENV = 'testing'
    TESTING = True
    DOMAIN = 'http://testserver'
    PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
    SQLALCHEMY_DATABASE_URI = 'sqlite:///%s' % (os.path.join(PROJECT_ROOT, "db.sqlite3"))
