# Python program to demonstrate
# shutil.copytree()


# imporing modules
import shutil
import os


# Declaring the ignore function
def ignoreFunc(file):
    def _ignore_(path, names):
        # List containing names of file
        # that are needed to ignore
        ignored = []

        # check if file in names
        # then add it to list
        if file in names:
            ignored.append(file)
        return set(ignored)

    return _ignore_


PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
# Source path
src = PROJECT_ROOT

# Destination path
dest = os.path.join(PROJECT_ROOT, 'build')

shutil.rmtree(dest)

# Copying the contents from Source
# to Destination without some
# specified files or directories
if __name__ == '__main__':
    shutil.copytree(src, dest,
                    ignore=shutil.ignore_patterns(
                        'ambvir',
                        '.idea',
                        '.git',
                        'build',
                        'build.py',
                        'manage.py',
                        'tests',
                        'migrations',
                        '__pycache__',
                        '.gitignore',
                        'requirements.txt')
                    )
